This is a quiz app in Flutter that asks questions about famous books with a timer for each question.
Here are some screenshots of the app.

![](images/screenshot1.PNG) ![](images/screenshot2.PNG)


![](images/screenshot3.PNG)


References: https://www.youtube.com/watch?v=yHrpx4PoBzU&feature=youtu.be
Images courtesy:
https://images-na.ssl-images-amazon.com/images/I/61elZCuOZ0L._SX365_BO1,204,203,200_.jpg
https://images-na.ssl-images-amazon.com/images/I/71xU1YzoqJL.jpg
https://images-na.ssl-images-amazon.com/images/I/91eKRbuhgaL.jpg
https://www.google.co.in/url?sa=i&url=https%3A%2F%2Fwww.quoteswave.com%2Fpicture-quotes%2F217509&psig=AOvVaw1K8QzYk92LXAFw7UgpfpxF&ust=1599023486234000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJD9kfSYx-sCFQAAAAAdAAAAABAD
