import 'package:flutter/material.dart';
import 'package:quizzer/quizpage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> images = [
    "images/prideandprejudice.jpg",
    "images/sherlockholmes.jpg",
    "images/thesecretgarden.jpg"
  ];

  Widget customCard(String title, String imagepath, String jsonPath) {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => GetJson(jsonPath)));
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Material(
            elevation: 10.0,
            child: Container(
              color: Theme.of(context).primaryColor,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 10.0,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Container(
                        color: Theme.of(context).accentColor,
                        width: 200,
                        child: Image(
                          image: AssetImage(
                            imagepath,
                          ), // to do
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontSize: 25.0,
                        fontFamily: "NanumGothic",
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Test your knowledge!",
          style: TextStyle(
            fontSize: 25.0,
            fontFamily: "NanumGothic",
          ),
        ),
      ),
      body: ListView(
        children: [
          customCard("Pride and Prejudice", images[0], "assets/first.json"),
          customCard("Sherlock Holmes", images[1], "assets/second.json"),
          customCard("The Secret Garden", images[2], "assets/third.json"),
        ],
      ),
    );
  }
}
