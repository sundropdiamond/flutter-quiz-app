import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:quizzer/resultpage.dart';

class GetJson extends StatelessWidget {
  String jsonPath;

  GetJson(this.jsonPath);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // future- where we load assets from
      future: DefaultAssetBundle.of(context).loadString(jsonPath),
      builder: (context, snapshot) {
        // builder: all FutureBuilder builders take context, snapshot
        List myData = json.decode(snapshot.data.toString());
        if (myData == null) {
          return Scaffold(
            body: Center(
              child: Text(
                "Loading",
              ),
            ),
          );
        } else {
          return QuizPage(
            myData: myData,
          );
        }
      },
    );
  }
}

class QuizPage extends StatefulWidget {
  var myData;

  QuizPage({@required this.myData});
  // A new widget will only be used to update an existing element if its key is the same as the key of the current widget associated with the element.
  @override
  _QuizPageState createState() => _QuizPageState(myData);
}

class _QuizPageState extends State<QuizPage> {
  var myData;
  int marks = 0;
  int quesno = 1;
  int time = 30;
  bool answerSelectable = true;

  _QuizPageState(this.myData);

  Color colorToShow = Colors.indigo;
  Color right = Colors.indigoAccent;
  Color wrong = Colors.pinkAccent;
  String showTimer = "30";
  bool cancelTimer = false;

  void checkAnswer(String k) {
    if (myData[2][quesno.toString()] == myData[1][quesno.toString()][k]) {
      setState(() {
        marks += 10;
        colorToShow = right;
      });
    } else {
      setState(() {
        colorToShow = wrong;
      });
    }
    setState(() {
      btnColor[k] = colorToShow;
      cancelTimer = true;
    });

    Timer(Duration(seconds: 2), nextQuestion);
  }

  void nextQuestion() {
    startTimer();
    setState(() {
      answerSelectable = true;
      cancelTimer = false;
      if (quesno < 3) {
        quesno++;
      } else {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => ResultPage(
              marks: marks,
            ),
          ),
        );
      }
      // if we don't reset colors buttons will be same
      // and color will carry over to next question
      btnColor["a"] = Colors.indigo;
      btnColor["b"] = Colors.indigo;
      btnColor["c"] = Colors.indigo;
      btnColor["d"] = Colors.indigo;
    });
  }

  Map<String, Color> btnColor = {
    "a": Colors.indigo,
    "b": Colors.indigo,
    "c": Colors.indigo,
    "d": Colors.indigo,
  };

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  void startTimer() async {
    setState(() {
      time = 30;
    });
    const oneSec = Duration(seconds: 1);
    Timer.periodic(oneSec, (Timer t) {
      setState(() {
        if (time < 1) {
          t.cancel();
          nextQuestion();
        } else if (cancelTimer) {
          t.cancel();
        } else {
          time -= 1;
        }
        showTimer = time.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return showDialog(
          context: context,
          child: AlertDialog(
            title: Text("LitQuizzer"),
            content: Text("You cannot exit without completing the quiz"),
            actions: [
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("OK"),
              )
            ],
          ),
        );
      },
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(15.0),
                alignment: Alignment.centerLeft,
                child: Text(
                  myData[0][quesno.toString()],
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontSize: 24.0,
                    fontFamily: "NanumGothic",
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 6,
              child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    choiceButton("a"),
                    choiceButton("b"),
                    choiceButton("c"),
                    choiceButton("d"),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                child: Center(
                  child: Text(
                    showTimer,
                    style: TextStyle(
                      fontSize: 35.0,
                      fontWeight: FontWeight.w700,
                      fontFamily: "NanumGothic",
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget choiceButton(String k) {
    return MaterialButton(
      color: btnColor[k],
      minWidth: 200.0,
      height: 45.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      onPressed: () {
        if (answerSelectable) {
          checkAnswer(k);
          setState(() {
            answerSelectable = false;
          });
        }
      },
      child: Text(
        myData[1][quesno.toString()][k],
        style: TextStyle(
          color: Colors.white,
          fontFamily: "NanumGothic",
          fontSize: 20.0,
        ),
      ),
    );
  }
}
