import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:quizzer/home.dart';
import 'package:splashscreen/splashscreen.dart';

class Splash extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SplashState();
  }
}

class _SplashState extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
        seconds: 3,
        navigateAfterSeconds: HomePage(),
        title: Text("LitQuizzer", style: TextStyle(
          fontSize: 50.0,
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontFamily: "LondrinaSolid",
          shadows: <Shadow>[
            Shadow(
              offset: Offset(2.0, 2.0),
            ),
          ],
        ),),
        backgroundColor: Theme.of(context).primaryColor,
        styleTextUnderTheLoader: TextStyle(
          color: Colors.white,
        ),
        loaderColor: Theme.of(context).secondaryHeaderColor,
    );

    /*Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        child: Center(
          child: Text(
            "LitQuizzer",
            style: TextStyle(
              fontSize: 50.0,
              fontWeight: FontWeight.w400,
              color: Colors.white,
              fontFamily: "LondrinaSolid",
              shadows: <Shadow>[
                Shadow(
                  offset: Offset(2.0, 2.0),
                ),
              ],
            ),
          ),
        ),
      ),
    );*/
  }
}
