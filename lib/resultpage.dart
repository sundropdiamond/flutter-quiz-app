import 'package:flutter/material.dart';
import 'package:quizzer/home.dart';

class ResultPage extends StatefulWidget {
  int marks;
  ResultPage({@required this.marks});
  @override
  _ResultPageState createState() => _ResultPageState(marks);
}

class _ResultPageState extends State<ResultPage> {
  int marks;
  List<String> images = ["images/good.jpeg", "images/ok.jpg"];
  String imagepath;

  _ResultPageState(this.marks);

  Widget score(int value){
    final textStyle = TextStyle(
      fontFamily: "LondrinaSolid",
      fontWeight: FontWeight.w600,
      fontSize: 85,
    );

    return Padding(
      padding: EdgeInsets.all(30),
      child: Text("$value", style: textStyle,),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (this.marks >= 20) {
      imagepath = images[0];
    } else {
      imagepath = images[1];
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Your score",
          style: TextStyle(
          fontSize: 25.0,
          fontFamily: "NanumGothic",
        ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(50.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                score(this.marks),
                Divider(
                  color: Colors.black,
                  thickness: 20,
                ),
                score(30),
              ],
            ),
            Center(
              child: FlatButton(
                padding: EdgeInsets.all(20),
                color: Theme.of(context).accentColor,
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePage(),
                    ),
                  );
                },
                child: Text(
                  "Back to home",
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}


